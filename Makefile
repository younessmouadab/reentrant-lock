#!/bin/sh
.SUFFIXES : .c .o


# CONSTANTES (g++ pour linux)
CC = g++
CFLAGS = -w -g -Wall
MAKE = make

all:
	$(MAKE) main
	$(MAKE) test
	
main: main.o lock.o
	$(CC) $(CFLAGS) -pthread -o main main.o lock.o
test: test.o lock.o
	$(CC) $(CFLAGS) -pthread -o test test.o lock.o

lock.o: lock.cpp lock.hpp
	$(CC) $(CFLAGS) -c lock.cpp

# CLEAN .o
clean:
	rm -f core *.o main
	rm -f core *.o test


