# Reentrant lock

  A reentrant lock is one where a process can claim the lock multiple times without blocking on itself, while preventing access to the same resource from other threads. The goal of this project is to implement a reentrant lock  and test it.

#Makefile:

- To compile main.cpp lock.cpp and lock.hpp : make main (./main for the execution)
- To compile test.cpp lock.cpp and lock.hpp : make test (./test for the execution)
- To compile all files : make all
- To delete all  *.o files : make clean


