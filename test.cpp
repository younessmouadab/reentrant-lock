#include <iostream>
#include <thread>
#include "lock.hpp"

#define PASSED "\033[1;32mPassed\033[0m\n"
#define FAILED "\033[1;31mFailed\033[0m\n"

void TestReentrantLockConstruct()
{
    // Test for Reentrant lock
    std::cout << "Test : Construct a ReentrantLock object"<< std::endl;
    bool ok = false;
    try
    {
    	ReentrantLock lock;
    	ok = true;
    }
   catch(...)
   {
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }

   std::cout << "Test " << (ok ? PASSED : FAILED) << std::endl;

}


void TestSharedClassConstruct()
{
    // Test for SharedClass
    std::cout << "Test : Construct a SharedClass object"<< std::endl;
    bool ok = false;
    try
    {
    	ReentrantLock lock;
        SharedClass sharedInstance(&lock);

    	ok = true;
    }
   catch(...)
   {
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }

   std::cout << "Test " << (ok ? PASSED : FAILED) << std::endl;

}


void TestLockUnlock()
{
    // Test LOck & unlock functions
    std::cout << "Test : Lock & Unlock functions "<< std::endl;
    bool ok = false;
    int a;
    try
    {
    	ReentrantLock r_lock;
	r_lock.lock();
	a = 15;
	r_lock.unlock();

    	ok = true;
    }
   catch(...)
   {
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }

   std::cout << "Test " << (ok && a ==15 ? PASSED : FAILED) << std::endl;

}

void TestTwoThreadsFunctionA()
{
    // Test two threads which run the same functionA
    std::cout << "Test : two threads which executing the same function A "<< std::endl;
    bool ok = false;
    ReentrantLock r_lock;
    SharedClass sharedInstance(&r_lock);
    try
    {


        std::thread t1(&SharedClass::functionA, &sharedInstance);
        std::thread t2(&SharedClass::functionA, &sharedInstance);

        t2.join();
        t1.join();

    	ok = true;
    }
   catch(...)
   {
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }

   std::cout << "Test " << (ok && sharedInstance.get_shared() == "functionA"? PASSED : FAILED) << std::endl;
}

void TestTwoThreadsFunctionB()
{
    // Test two threads which run the same functionB
    std::cout << "Test : two threads which executing the same function A "<< std::endl;
    bool ok = false;
    ReentrantLock r_lock;
    SharedClass sharedInstance(&r_lock);
    try
    {


        std::thread t1(&SharedClass::functionB, &sharedInstance);
        std::thread t2(&SharedClass::functionB, &sharedInstance);

        t2.join();
        t1.join();

    	ok = true;
    }
   catch(...)
   {
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }
}
int factorial(int n, Lock &r_lock){
	// calculate the factorial.
	int result;
	r_lock.lock();
	if ( n < 0) result=0;
	else if ( n==0 || n==1) result=1;
	else result= n*factorial(n-1, r_lock);
	r_lock.unlock();
	return result;

}

void TestFactorialFunction(int n, int RESULT){
   // Test factorial function with lock and unlock methods
    std::cout << "Test : factorial function "<< std::endl;
    bool ok = false;
    int result;
    ReentrantLock r_lock;
    try
    {   

	result = factorial(n, r_lock);
        ok = true;
    }
   catch(...)
   {
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }

   std::cout << "Test " << (ok && result == RESULT ? PASSED : FAILED) << std::endl;
}

void CallFactorial(int n,  int RESULT){

	int result;
	ReentrantLock r_lock;
	result = factorial(n, r_lock);
	std::cout << "the result "<< result <<" the expected result " << RESULT << std::endl;
}

void TestOneThread(){
   // Test a thread executing factorial function
    std::cout << "Test : on thread which executs a reccursive function: factorial"<< std::endl;
    bool ok = false;
    int RESULT = 120;
       int n =5;
    try
    {   

        std::thread t1(&CallFactorial, n, RESULT);

        t1.join();

        ok = true;
    }   
   catch(...)
   {   
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }   

   std::cout << "Test " << (ok ? PASSED : FAILED) << std::endl;
}

void TestThreadsFactorial(){
   // Test two threads which execute the same function with different parametres!
    std::cout << "Test : two threads which execut a reccursive function: factorial"<< std::endl;
    bool ok = false;

    int RESULT1 = 120;
    int n1 =5; 
    int RESULT2 = 24;
    int n2 =4;

    try
    {   

        std::thread t1(&CallFactorial, n1, RESULT1); 
        std::thread t2(&CallFactorial, n2, RESULT2); 

        t1.join();
        t2.join();

        ok = true;
    }
   catch(...)
   {   
      // It's dangerous, TODO implement differnet exceptions !
      std::cout << "Unexpected exception" << std::endl;

      ok = false;
   }   

   std::cout << "Test " << (ok ? PASSED : FAILED) << std::endl;
}

int main()
{
	TestReentrantLockConstruct();
	TestSharedClassConstruct();
	TestLockUnlock();
	TestTwoThreadsFunctionA();
	TestTwoThreadsFunctionB();
	TestFactorialFunction(5,120);
	TestOneThread();
	TestThreadsFactorial();
	return 0;

}

