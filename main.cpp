#include "lock.hpp"

int main() 
{
    ReentrantLock a;

    SharedClass sharedInstance(&a);
    
    std::thread t1(&SharedClass::functionA, &sharedInstance);
    std::thread t2(&SharedClass::functionB, &sharedInstance);

    t2.join();
    t1.join();
}

