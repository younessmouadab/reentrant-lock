
#include "lock.hpp"



void ReentrantLock::lock()
{
	while(is_locked && id_thread != std::this_thread::get_id()){
		mtx.lock();
	}
	id_thread = std::this_thread::get_id();
	is_locked = true;
	count++;
}

void ReentrantLock::unlock()
{
	if (!is_locked || id_thread != std::this_thread::get_id()) {
            return;
        }
        count--;
        if(count == 0){
            is_locked = false;
            mtx.unlock();
        }
}

void SharedClass::functionA() {
  lock->lock();

  shared = "functionA";
  std::cout << "in functionA, shared variable is now " << shared << '\n';
  
  lock->unlock();
}

void SharedClass::functionB() {
  lock->lock();

  shared = "functionB";
  std::cout << "in functionB, shared variable is now " << shared << '\n';
  functionA();
  std::cout << "back in functionB, shared variable is " << shared << '\n';

  lock->unlock();
}

std::string SharedClass::get_shared() {
	return shared;
}
