#ifndef LOCK_H
#define LOCK_H

#include <iostream>
#include <mutex>
#include <unistd.h>
#include <thread>
#include <memory>

class Lock {
	protected:
        	std::mutex mtx;
	public:
	       	virtual void lock() = 0;
		virtual void unlock() = 0;
};

class ReentrantLock: public Lock {

	private:
		int count = 0;
		bool is_locked = false;
		std::thread::id id_thread;

	public:

   		void lock();
   		void unlock();

};


class SharedClass {

public:

    SharedClass(Lock * LOCK) {
    lock = LOCK;
    }

    void functionA();

    void functionB();

    std::string get_shared();

private:
    std::string shared;
    Lock * lock;
};

#endif
